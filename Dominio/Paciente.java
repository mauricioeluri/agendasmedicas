package Dominio;

/**
 *
 * @author Huillian Serpa, Maurício El Uri
 */
public class Paciente extends Pessoa{
    
    private int idade;
    private long nSus;

    /**
     * CONSTRUTOR DA CLASSE PACIENTE
     * @param idade
     * @param nSus
     * @param nome
     * @param email
     * @param Senha
     * @param telefone
     * @param cpf 
     */
    public Paciente(int idade, long nSus, String nome, String email, String Senha, long telefone, long cpf) {
        super(nome, email, Senha, telefone, cpf);
        this.idade = idade;
        this.nSus = nSus;
    }

    /**
     * @return the idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * @param idade the idade to set
     */
    public void setIdade(int idade) {
        this.idade = idade;
    }

    /**
     * @return the nSus
     */
    public long getnSus() {
        return nSus;
    }

    /**
     * @param nSus the nSus to set
     */
    public void setnSus(long nSus) {
        this.nSus = nSus;
    }

    public boolean agendarConsulta(){
        return true;
    }

    public boolean pesquisarHorario(){
        return true;
    }
}
