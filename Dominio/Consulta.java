package Dominio;

/**
 *
 * @author Huillian Serpa, Maurício El Uri
 */
public class Consulta {

    private Medico medico;
    private Paciente paciente;
    private int horaInicio;
    private int horaFim;

    public Consulta(Medico medico, Paciente paciente, int horaInicio, int horaFim) {
        this.medico = medico;
        this.paciente = paciente;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    /**
     * @return the medico
     */
    public Medico getMedico() {
        return medico;
    }

    /**
     * @param medico the medico to set
     */
    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    /**
     * @return the paciente
     */
    public Paciente getPaciente() {
        return paciente;
    }

    /**
     * @param paciente the paciente to set
     */
    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    /**
     * @return the horaInicio
     */
    public int getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param horaInicio the horaInicio to set
     */
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * @return the horaFim
     */
    public int getHoraFim() {
        return horaFim;
    }

    /**
     * @param horaFim the horaFim to set
     */
    public void setHoraFim(int horaFim) {
        this.horaFim = horaFim;
    }
    
    
}
