package Dominio;

/**
 *
 * @author Huillian Serpa, Maurício El Uri
 */
public class Pessoa {

    private String nome;
    private String email;
    private String Senha;
    private long telefone;
    private long cpf;

    /**
     * CONSTRUTOR DA CLASSE PESSOA
     * @param nome
     * @param email
     * @param Senha
     * @param telefone
     * @param cpf 
     */
    public Pessoa(String nome, String email, String Senha, long telefone, long cpf) {
        this.nome = nome;
        this.email = email;
        this.Senha = Senha;
        this.telefone = telefone;
        this.cpf = cpf;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the Senha
     */
    public String getSenha() {
        return Senha;
    }

    /**
     * @param Senha the Senha to set
     */
    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    /**
     * @return the telefone
     */
    public long getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(long telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the cpf
     */
    public long getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(long cpf) {
        this.cpf = cpf;
    }

    
    public boolean verificarAgenda(){
        return true;
    }
}
