package Dominio;

/**
 *
 * @author Huillian Serpa, Maurício El Uri
 */
public class Medico extends Pessoa {

    private long crm;
    private String especializacao;

    /**
     * CONSTRUTOR DA CLASSE MÉDICO
     * @param crm
     * @param especializacao
     * @param nome
     * @param email
     * @param Senha
     * @param telefone
     * @param cpf 
     */
    public Medico(long crm, String especializacao, String nome, String email, String Senha, long telefone, long cpf) {
        super(nome, email, Senha, telefone, cpf);
        this.crm = crm;
        this.especializacao = especializacao;
    }

    /**
     * @return the crm
     */
    public long getCrm() {
        return crm;
    }

    /**
     * @param crm the crm to set
     */
    public void setCrm(long crm) {
        this.crm = crm;
    }

    /**
     * @return the especializacao
     */
    public String getEspecializacao() {
        return especializacao;
    }

    /**
     * @param especializacao the especializacao to set
     */
    public void setEspecializacao(String especializacao) {
        this.especializacao = especializacao;
    }

    
    public boolean confirmarConsulta(){
        return true;
    }
}
